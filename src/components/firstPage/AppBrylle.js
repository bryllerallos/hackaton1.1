import React from "react";
import "../../../src/App.css";
import Navbar from "./Navbar";

const AppBrylle = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className="container">
        <div className="section-one">
          <img id="hands" src="./handwash.png" alt="" />
          <h1>Wash your Hands Frequently</h1>
          <p>
            Regularly and thouroughly clean your hands with an alcohol-based had
            rub or wash them with soap and water.
          </p>
         
        </div>
        <div className="section-two">
          <img id="id" src="./distance.png" alt="" />
          <h1>Keep Distance</h1>
          <p>
            Maintain atleast a distance of 1 meter or 3 feet to anyone
            especially those with cough and colds.
          </p>
        </div>
        <div className="section-three">
          <img id="face" src="./face.png" alt="" />
          <h1>Avoid Touching Your Face</h1>
          <p>
            Your hands can pick up viruses when you touch something and it can
            travel to your eyes, nose and mouth. As much as possible avoid
            touching your face.
          </p>
        </div>
        <div className="section-four">
          <img id="doctor" src="./doctor.png" alt="" />
          <h1>Seek Medical Assistance</h1>
          <p>
            For symptoms in relation to corona virus dont hesitate to seek
            medical assistance.
          </p>
        </div>
      </div>
     
    </React.Fragment>
  );
};

export default AppBrylle;
