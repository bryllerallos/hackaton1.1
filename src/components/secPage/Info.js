import React, { useState } from "react";
import { FormGroup, Label, Input, Button } from "reactstrap";

const Info = props => {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [address, setAddress] = useState("");

  const addName = e => {
    setName(e.target.value);
  };
  const addAge = e => {
    setAge(e.target.value);
  };
  const addAddress = e => {
    setAddress(e.target.value);
  };
  // console.log(name, age, address);

  return (
    // <FormGroup>
    //   {props.informations.map((info, index) => (
    //     <Label key={index}>{info}</Label>
    //     // <Input placeholder="Enter your name" />
    //   ))}
    // </FormGroup>

    <React.Fragment>
    <FormGroup
      style={{
        backgroundColor: "#56ccf2"
      }}
    >
      <Label>Name:</Label>
      <Input placeholder="Enter your name" onChange={addName} />
      <Label>Age:</Label>
      <Input placeholder="Enter your age" onChange={addAge} />
      <Label>Address:</Label>
      <Input placeholder="Enter your address" onChange={addAddress} />
     
    </FormGroup>
    <Button 
       className='btn btn-info'
       style={{marginRight:'20px'}} 
      >Submit</Button>
    </React.Fragment>
  );
};
export default Info;
