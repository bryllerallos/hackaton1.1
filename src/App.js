import React, { useState } from "react";
// import Navbar from "./components/firstPage/Navbar";
import Survey from "./components/secPage/Survey";
import Tabulation from "./components/thirdPage/Tabulation";
import AppBrylle from "./components/firstPage/AppBrylle";

const App = props => {
  const [informations, setInformations] = useState([
    {
      name: "Ryan",

      age: 28,

      address: "Jan sa Kanto"
    },
    {
      name: "Richard",

      age: 34,

      address: "Kabilang kanto"
    }
  ]);

  const [surveys, setSurveys] = useState([
    {
      question: "Have you travel abroad for the past two weeks?"
    },
    {
      question: "Are you feeling sick?"
    },
    {
      question: "Do you usually go outside?"
    },
    {
      question: "Do you smoke?"
    }
  ]);

  const [answers, setAnswers] = useState(["Yes", "No"]);
  // console.log(informations);

  // handleSaveInformation = (name,age, address, e) => {
  //   let newName = {e.target.}
  // }

  return (
    <React.Fragment>
      <div>
        <AppBrylle />
      </div>
      <div className="vh-100 my-5 ">
        <Survey
          surveys={surveys}
          answers={answers}
          informations={informations}
        />
      </div>
      <div className='vh-100 py-5'>
        <Tabulation informations={informations} />
      </div>
      <footer>
        <p id="source">Source: World Health Organization</p>
      </footer>
    </React.Fragment>
  );
};

export default App;
